#  scripts
# importar librerías
from selenium import webdriver
import time 

#  script#1: validar selección
seleniumdriver = webdriver.Chrome(executable_path='pruebaqa\qaback\SuiteTest\chromedriver.exe')
seleniumdriver.get("http://127.0.0.1:8000/polls/1/")
seleniumdriver.find_element_by_id('choice1').submit()
time.sleep(5) # Delay for 5 seconds.
seleniumdriver.close()

#  script#2: seleccionar opciones y enviar vote
seleniumdriver = webdriver.Chrome(executable_path='pruebaqa\qaback\SuiteTest\chromedriver.exe')
seleniumdriver.get("http://127.0.0.1:8000/polls/1/")
seleniumdriver.find_element_by_id('choice1').click()
time.sleep(2) # Delay for 2 seconds.
seleniumdriver.find_element_by_id('choice2').click()
time.sleep(2) # Delay for 2 seconds.
seleniumdriver.find_element_by_id('choice3').click()
time.sleep(2) # Delay for 2 seconds.
seleniumdriver.find_element_by_id('choice1').submit()
time.sleep(2) # Delay for 2 seconds.
seleniumdriver.close()

#  script#3: vote again por cada opción
seleniumdriver = webdriver.Chrome(executable_path='pruebaqa\qaback\SuiteTest\chromedriver.exe')
seleniumdriver.get("http://127.0.0.1:8000/polls/1/")
seleniumdriver.find_element_by_id('choice1').click()
seleniumdriver.find_element_by_id('choice1').submit()
seleniumdriver.find_element_by_link_text('Vote again?').click()
time.sleep(3) # Delay for 3 seconds.
seleniumdriver.find_element_by_id('choice2').click()
seleniumdriver.find_element_by_id('choice2').submit()
seleniumdriver.find_element_by_link_text('Vote again?').click()
time.sleep(3) # Delay for 3 seconds.
seleniumdriver.find_element_by_id('choice3').click()
seleniumdriver.find_element_by_id('choice3').submit().click()
time.sleep(3) # Delay for 3 seconds.
seleniumdriver.close()

#  script#4: vote again por cada opción
seleniumdriver = webdriver.Chrome(executable_path='pruebaqa\qaback\SuiteTest\chromedriver.exe')
seleniumdriver.get("http://127.0.0.1:8000/polls/1/")
seleniumdriver.find_element_by_id('choice1').click()
seleniumdriver.find_element_by_id('choice1').submit()
seleniumdriver.find_element_by_link_text('Vote again?').click()
time.sleep(3) # Delay for 3 seconds.
seleniumdriver.find_element_by_id('choice2').click()
seleniumdriver.find_element_by_id('choice2').submit()
seleniumdriver.find_element_by_link_text('Vote again?').click()
time.sleep(3) # Delay for 3 seconds.
seleniumdriver.find_element_by_id('choice3').click()
seleniumdriver.find_element_by_id('choice3').submit().click()
time.sleep(3) # Delay for 3 seconds.
seleniumdriver.close()

